package pl.codementors.library;

import java.io.Serializable;

public class Book implements Serializable {

    private Author[] authors = new Author[10];

    public void setAuthors(Author[] authors) {
        this.authors = authors;
    }

    public Author[] getAuthors() {
        return authors;
    }

    private String title;

    private int publishYear;

    public String getTitle() {
        return title;
    }

    public int getPublishYear() {
        return publishYear;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublishYear(int publishYear) {
        this.publishYear = publishYear;
    }

    public void printBook() {
        System.out.println("---------------------");
        System.out.println("Tytuł: " +title);

        for (int i = 0; i < authors.length; i++) {

            System.out.println("");
            System.out.println("("+(i+1)+")");
            System.out.println("Autor: " + authors[i].getName());
            System.out.println("Nazwisko: " + authors[i].getSurname());
            System.out.println("Pseudonim: " + authors[i].getAlias());
            System.out.println("typ" + getClass().getSimpleName());
        }
        System.out.println("");
        System.out.println("Data wydania: " +publishYear);
        System.out.println("---------------------");
    }
}