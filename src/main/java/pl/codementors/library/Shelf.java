package pl.codementors.library;

import java.io.Serializable;

public class Shelf implements Serializable {

   private Book[] books = new Book[10];

   public Book getBook (int index) {
       return books[index];
   }

   public void setBook (int index, Book book) {
       books[index] = book;
   }

   public void print () {

       for (int i = 0; i<10; i++) {
           if (getBook(i) != null) {
               getBook(i).printBook();
           }
       }
   }
}