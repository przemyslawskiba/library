package pl.codementors.library;
import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Library library = new Library();

        boolean run = true;

        System.out.println("Command:");
        System.out.println("add");
        System.out.println("print");
        System.out.println("print_all");
        System.out.println("save");
        System.out.println("read");
        System.out.println("quit");

        while (run) {

            String text = scanner.nextLine();

            switch (text) {

                case "add": {

                    Book newbook = null;

                    System.out.println("Podaj rodzaj czegos tam");
                    String type = scanner.nextLine();

                    if (type.equals("magazine")) {
                        System.out.println("podaj date wydania YYYY-MM-DD");
                        String date = scanner.nextLine();
                        Magazine magazine = new Magazine();
                        magazine.setDate(date);
                        newbook = magazine;

                    }

                    if (type.equals("book")) {
                        System.out.println("podaj rok i nacisnij enter");
                        int publishYear = scanner.nextInt();
                        newbook = new Book();
                        newbook.setPublishYear(publishYear);
                    }

                    if (type.equals("comicbook")) {
                        System.out.println("podaj date wydania YYYY-MM-DD");
                        String date = scanner.nextLine();
                        System.out.println("podaj serie wydwaniczą");
                        String series = scanner.nextLine();
                        Comicbook comicbook = new Comicbook();
                        comicbook.setDate(date);
                        comicbook.setSeries(series);
                        newbook = comicbook;
                    }


                    System.out.println("Podaj regał i nacisnij enter");
                    int bookstand = scanner.nextInt();
                    scanner.skip("\n");
                    System.out.println("podaj półkę i nacisnij enter");
                    int shelf = scanner.nextInt();
                    scanner.skip("\n");
                    System.out.println("podaj index i nacisnij enter");
                    int index = scanner.nextInt();
                    scanner.skip("\n");

                        System.out.println("podaj tytul i nacisnij enter");
                        String title = scanner.nextLine();
                        System.out.println("podaj liczbę autorów: ");
                        int numberAuthors = scanner.nextInt();
                        scanner.skip("\n");


                        Author[] arrAuthor = new Author[numberAuthors];
                        for (int i = 0; i < arrAuthor.length; i++) {

                            System.out.println("podaj imie autora i nacisnij enter");
                            String name = scanner.nextLine();
                            System.out.println("podaj nazwisko autora i nacisnij enter");
                            String surname = scanner.nextLine();
                            System.out.println("podaj pseudonim autora i nacisnij enter");
                            String alias = scanner.nextLine();

                            Author author = new Author();

                            author.setName(name);
                            author.setSurname(surname);
                            author.setAlias(alias);
                            arrAuthor[i] = author;
                        }

                                newbook.setTitle(title);
                                newbook.setAuthors(arrAuthor);

                    if (library.getBookstand(bookstand) == null) {

                        Bookstand newBookstand = new Bookstand();
                        library.setBookstand(bookstand, newBookstand);
                    }

                    if (library.getBookstand(bookstand).getShelf(shelf) == null) {

                        Shelf newShelf = new Shelf();
                        library.getBookstand(bookstand).setShelf(shelf, newShelf);
                    }

                    library.getBookstand(bookstand).getShelf(shelf).setBook(index, newbook);
                    break;
                }

                case "quit": {

                    run = false;
                    break;
                }

                case "save": {
                    System.out.println("zapisuje do pliku...");
                    //library.writeAllBooksToFile();
                    writeAllNotesToBinaryFile(library);

                    break;
                }

                case "print": {

                    System.out.println("Podaj regał i nacisnij enter");
                    int bookstand = scanner.nextInt();
                    scanner.skip("\n");
                    System.out.println("podaj półkę i nacisnij enter");
                    int shelf = scanner.nextInt();
                    scanner.skip("\n");
                    System.out.println("podaj index i nacisnij enter");
                    int index = scanner.nextInt();
                    scanner.skip("\n");
                    System.out.print(index);

                    if (library.getBookstand(bookstand) != null
                            && library.getBookstand(bookstand).getShelf(shelf) != null) {

                        library.getBookstand(bookstand).getShelf(shelf).getBook(index).printBook();
                    } else {
                        System.out.println("pusta");
                    }
                    break;

                }

                case "print_all": {

                    for (int i = 0; i < 10; i++) {

                        if (library.getBookstand(i) != null) {
                            System.out.println(i);
                            library.getBookstand(i).print();
                        }
                    }
                    break;
                }

                case "read": {
                    //library.readAllNotesFromFile();
                    library = readAllNotesFromBinaryFile();
                    System.out.println("wczytuje z pliku..");
                    break;
                }
            }
        }
    }

    public static void writeAllNotesToBinaryFile(Library library) {
        try (FileOutputStream fos = new FileOutputStream("/tmp/books.bin");//In future maybe change it to some user dir.
             ObjectOutputStream oos = new ObjectOutputStream(fos);) {
            oos.writeObject(library);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static Library readAllNotesFromBinaryFile() {
        try (FileInputStream fis = new FileInputStream("/tmp/books.bin");//In future maybe change it to some user dir.
             ObjectInputStream ois = new ObjectInputStream(fis);) {
            Library library = (Library) ois.readObject();
            return library;
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }
}