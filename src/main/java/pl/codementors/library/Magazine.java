package pl.codementors.library;

/**
 * Created by pskiba on 12.06.2017.
 */
public class Magazine extends Book {

    private String date;

    private String edition;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    @Override
    public void printBook() {
        System.out.println("---------------------");
        System.out.println("Tytuł: " + getTitle());

        for (int i = 0; i < getAuthors().length; i++) {
            System.out.println("");
            System.out.println("(" + (i + 1) + ")");
            System.out.println("Autor: " + getAuthors()[i].getName());
            System.out.println("Nazwisko: " + getAuthors()[i].getSurname());
            System.out.println("Pseudonim: " + getAuthors()[i].getAlias());
            System.out.println("typ" + getClass().getSimpleName());
        }
        System.out.println("Data wydania" + date);
    }
}
