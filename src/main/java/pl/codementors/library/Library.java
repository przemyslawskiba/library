package pl.codementors.library;

import java.io.*;
import java.util.Scanner;

public class Library implements Serializable {

    private Bookstand[] bookstands = new Bookstand[10];

    public Bookstand getBookstand(int index) {
        return bookstands[index];
    }

    public void setBookstand(int index, Bookstand bookstand) {
        bookstands[index] = bookstand;
    }

    /*
    public void writeAllBooksToFile() {
        try (FileWriter fw = new FileWriter("/tmp/books");
             BufferedWriter bw = new BufferedWriter(fw);) {
            int count = 0;//Number of notes to store in file.

            for (int i = 0; i < 10; i++) {
                if (getBookstand(i) != null) {
                    for (int j = 0; j < 10; j++) {
                        if (getBookstand(i).getShelf(j) != null) {
                            for (int k = 0; k < 10; k++) {
                                if (getBookstand(i).getShelf(j).getBook(k) != null) {
                                    count++;
                                }
                            }
                        }
                    }
                }
            }


            bw.write(count + "");
            bw.newLine();
            for (int i = 0; i < 10; i++) {
                if (getBookstand(i) != null) {
                    for (int j = 0; j < 10; j++) {
                        if (getBookstand(i).getShelf(j) != null) {
                            for (int k = 0; k < 10; k++) {
                                if (getBookstand(i).getShelf(j).getBook(k) != null) {

                                    bw.write(i + " " + j + " " + k + " " + getBookstand(i).getShelf(j).getBook(k).getTitle());
                                    bw.newLine();
                                    bw.write(getBookstand(i).getShelf(j).getBook(k).getAuthor());
                                    bw.newLine();
                                    bw.write(" " + getBookstand(i).getShelf(j).getBook(k).getPublishYear());
                                    bw.newLine();
                                }
                            }
                        }
                    }
                }
            }


        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }


    public void readAllNotesFromFile() {
        try (
                FileInputStream fr = new FileInputStream("/tmp/books");
                BufferedReader reader = new BufferedReader(new InputStreamReader(fr));


                Scanner scanner = new Scanner(reader);) {

                //String line = reader.readLine();

                int count = scanner.nextInt();

            for (int i = 0; i < count; i++) {


                int index = scanner.nextInt();
                System.out.println("index= " +index);
                int shelf = scanner.nextInt();
                System.out.println("półka= " +shelf);
                int bookstand = scanner.nextInt();
                System.out.println("regał= " +bookstand);
                String title = scanner.nextLine();
                System.out.println("Tytuł= " +title);
                String  author = scanner.nextLine();
                System.out.println("Autor= " +author);
                int year = scanner.nextInt();
                System.out.println("Rok= " +year);

                Book book = new Book();
                book.setTitle(title);
                book.setAuthor(author);
                book.setPublishYear(year);

                Library tempLibrary = new Library();

                if (tempLibrary.getBookstand(bookstand) == null) {

                    Bookstand newBookstand = new Bookstand();
                    tempLibrary.setBookstand(bookstand, newBookstand);
                }

                if (tempLibrary.getBookstand(bookstand).getShelf(shelf) == null) {

                    Shelf newShelf = new Shelf();
                    tempLibrary.getBookstand(bookstand).setShelf(shelf, newShelf);
                }

                tempLibrary.getBookstand(bookstand).getShelf(shelf).setBook(index, book);

            }
        }
        catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    */
}