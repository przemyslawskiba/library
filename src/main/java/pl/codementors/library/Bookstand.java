package pl.codementors.library;

import java.io.Serializable;

public class Bookstand implements Serializable {

    private Shelf[] shelfs = new Shelf[10];

    public Shelf getShelf (int index) {
        return shelfs[index];
    }

    public void setShelf (int index, Shelf shelf) {
        shelfs[index] = shelf;
    }

    public void print () {

        for (int i = 0; i<10; i++) {
            if (getShelf(i) != null) {
                getShelf(i).print();
            }
        }
    }
}
