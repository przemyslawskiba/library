package pl.codementors.library;

import java.io.Serializable;

public class Author implements Serializable {

    private String name;

    private String surname;

    private  String alias;

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAlias() {
        return alias;
    }

}

